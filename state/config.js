import api from '@/api'
import Vue from 'vue'

export const state = {
	config: {},
	content: {},
	name: ''
}

export const getters = {
	getContentByType: state => type => {
		return state.content[type]
	}
}

export const actions = {
	async initPage({ commit }, { type, id }) {
		let configResponse = await api.getPageConfig(type, id)
		commit('setPageConfig', configResponse.data)
		commit('setName', configResponse.data.meta.pageTitle)
	},
	async getContent({ commit }, { request, type }) {
		let response = await api.searchNews(request)
		commit('setPageContent', { content: response.data, type })
	}
}

export const mutations = {
	setPageConfig(state, config) {
		state.config = config
	},
	setName(state, name) {
		state.name = name
	},
	setPageContent(state, { content, type }) {
		Vue.set(state.content, type, content)
	}
}