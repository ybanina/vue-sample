function getTitle (vm) {
	const { pageTitle } = vm
	if (pageTitle) {
		return typeof pageTitle === 'function'
			? pageTitle.call(vm)
			: pageTitle
	}
}

export default {
	updated () {
		const pageTitle = getTitle(this)
		if (pageTitle) {
			document.title = pageTitle
		}
	}
}